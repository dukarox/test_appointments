<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::middleware('citizen')->group(function () {
    Route::get('/citizen', 'CitizenController@index')->name('citizen');
    Route::post('/appointment/save', 'AppointmentsController@save')->name('sendAppointment');
});

Route::middleware('lawyer')->group(function () {
    Route::get('/lawyer', 'LawyerController@index')->name('lawyer');
    Route::get('/approve/{id}', 'AppointmentsController@approve')->name('approveAppointment');
    Route::get('/decline/{id}', 'AppointmentsController@decline')->name('declineAppointment');
    Route::post('/appointment/edit', 'AppointmentsController@edit')->name('editAppointment');

});
