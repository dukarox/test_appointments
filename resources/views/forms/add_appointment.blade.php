<form method="POST" action="{{ route('sendAppointment') }}">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Lawyer</label>
        <div class="col-md-6">
            <select id="lawyer" name="lawyer_id" class="selectpicker">
                @foreach ($lawyers as $lawyer)
                    <option value="{{ $lawyer->id }}">{{ $lawyer->name }}</option>
                @endforeach
            </select>

            @error('lawyer_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Date</label>
        <div class="col-sm-6">
            <input type="date" name="date" class="form-control" required/>
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Time</label>
        <div class="col-sm-6">
            <input id="timepicker" type="time" name="time" class="form-control" required />
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">Send</button>
        </div>
    </div>
</form>
