@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert alert-info alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Your appointments</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('lawyer') }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="search" class="form-control" value="{{ $filter }}" placeholder="Search">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>

                    <div id="resheduleContainer" style="display: none;">
                        <div class="col-md-6 offset-md-3">
                            <h3>Reschedule appointment:</h3>
                        </div>
                        <form method="POST" action="{{ route('editAppointment') }}">
                            @csrf
                            <input type="hidden" id="appointment_id" name="appointment_id" value="">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Date</label>
                                <div class="col-sm-6">
                                    <input type="date" id="date" name="date" class="form-control" required/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Time</label>
                                <div class="col-sm-6">
                                    <input id="time" type="time" name="time" class="form-control" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3 offset-md-3">
                                    <button type="submit" class="btn btn-primary">Reschedule</button>
                                </div>
                                <div class="col-md-3">
                                    <button id="cancel_reschedule" type="button" class="btn btn-info">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@sortablelink('citizen.name', 'Citizen')</th>
                            <th scope="col">@sortablelink('appoint_date', 'Date & Time')</th>
                            <th scope="col">@sortablelink('created_at', 'Created')</th>
                            <th scope="col">@sortablelink('status', 'Status')</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($appointments->count() > 0)
                            @foreach ($appointments as $appointment)
                            <tr>
                                <th class="appoint_id" scope="row">{{ $appointment->id }}</th>
                                <td>{{ $appointment->citizenName() }}</td>
                                <td class="appoint_date" data-date="{{ $appointment->appoint_date }}">
                                    {{ $appointment->appoint_date }}
                                    @if (in_array($appointment->appoint_date, $duplicates))
                                        (duplicated)
                                    @endif
                                </td>
                                <td>{{ $appointment->created_at }}</td>
                                <td>
                                    @switch($appointment->status)
                                        @case(\App\Appointment::WAITING)
                                        Waiting
                                        @break
                                        @case(\App\Appointment::APPROVED)
                                        Approved
                                        @break
                                        @case(\App\Appointment::DECLINED)
                                        Declined
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    @if (in_array($appointment->appoint_date, $duplicates))
                                        <div class="col-xs-4">
                                            <button class="btn btn-warning reschedule">Reschedule</button>
                                        </div>
                                    @elseif ($appointment->status == \App\Appointment::WAITING)
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <form action="{{ route('approveAppointment', $appointment->id)}}" method="get" onclick="return confirm('Are you sure you want to confirm this appointment?');">
                                                    @csrf
                                                    <button class="btn btn-info" type="submit">Approve</button>
                                                </form>
                                            </div>
                                            <div class="col-xs-4">
                                                <form action="{{ route('declineAppointment', $appointment->id)}}" method="get" onclick="return confirm('Are you sure you want to decline this appointment?');">
                                                    @csrf
                                                    <button class="btn btn-danger" type="submit">Decline</button>
                                                </form>
                                            </div>
                                            <div class="col-xs-4">
                                                <button class="btn btn-warning reschedule">Reschedule</button>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                        <div class="col-xs-4">
                                            <button class="btn btn-warning reschedule">Reschedule</button>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="6">No appointments to display.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="text-center">{{ $appointments->links() }}</div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.reschedule').click(function () {
                $('#resheduleContainer').show();
                let tr = $(this).closest('tr')
                let appontmentId = tr.find('.appoint_id').first().html();
                let appontmentDate = tr.find('.appoint_date').first().attr('data-date');
                let date = new Date(appontmentDate);
                let timeOnly = ("0" + date.getHours()).slice(-2) + ':' + ("0" + date.getMinutes()).slice(-2);
                let dateOnly = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);

                $('#appointment_id').val(appontmentId);
                $('#time').val(timeOnly);
                $('#date').val(dateOnly);
            });

            $('#cancel_reschedule').click(function() {
                $('#resheduleContainer').hide();
            });
        });
    </script>
@endsection
