@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(Session::has('message'))
                <div class="alert alert-info alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add lawyer appointment</div>

                <div class="card-body">
                    @include('forms.add_appointment')
                </div>
            </div>

            <div class="card">
                <div class="card-header">My appointments</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('citizen') }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="search" class="form-control" value="{{ $filter }}" placeholder="Search">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">@sortablelink('lawyer.name', 'Lawyer')</th>
                                <th scope="col">@sortablelink('appoint_date', 'Date & Time')</th>
                                <th scope="col">@sortablelink('created_at', 'Created')</th>
                                <th scope="col">@sortablelink('status', 'Status')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($appointments->count() > 0)
                                @foreach ($appointments as $appointment)
                                <tr>
                                    <th scope="row">{{ $appointment->id }}</th>
                                    <td>{{ $appointment->lawyerName() }}</td>
                                    <td>{{ $appointment->appoint_date }}</td>
                                    <td>{{ $appointment->created_at }}</td>
                                    <td>
                                        @switch($appointment->status)
                                            @case(\App\Appointment::WAITING)
                                                Waiting
                                            @break
                                            @case(\App\Appointment::APPROVED)
                                                Approved
                                            @break
                                            @case(\App\Appointment::DECLINED)
                                                Declined
                                            @break
                                        @endswitch
                                    </td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="6">No appointments to display.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="text-center">{{ $appointments->links() }}</div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $("#timepicker").change( function(){
                let selectedTime = $(this).val();
                let newTimeWithoutMins = selectedTime.slice(0, -2) + '00';
                $(this).val(newTimeWithoutMins);
            });
        });
    </script>
@endsection
