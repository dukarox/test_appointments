<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\User;
use Auth;
use Illuminate\Http\Request;

class CitizenController extends Controller
{
    public function index(Request $request)
    {
        $lawyers = User::getLawyers();
        $appointments = Appointment::sortable()->where('citizen_id', Auth::user()->id);

        if ($request->has('search')) {
            $filter = $request->get('search');
            $appointments = $appointments->whereHas('lawyer', function ($query) use ($filter) {
                return $query->where('name', 'like', '%'.$filter.'%');
            });
        }

        $appointments = $appointments->paginate(5);

        return view('citizen_home', compact(
            'lawyers',
            'appointments',
            'filter'
        ));
    }
}
