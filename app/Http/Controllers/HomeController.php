<?php

namespace App\Http\Controllers;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::user()) {
            return redirect()->route('login');
        }

        if (Auth::user()->isLawyer()) {
            return redirect()->route('lawyer');
        } else {
            return redirect()->route('citizen');
        }
    }
}
