<?php

namespace App\Http\Controllers;

use App\Appointment;
use Auth;
use DB;
use Illuminate\Http\Request;

class LawyerController extends Controller
{

    public function index(Request $request)
    {
        $filter = '';

        $appointments = Appointment::sortable()
            ->where('lawyer_id', Auth::user()->id)
            ->orderBy('appoint_date');

        if ($request->has('search')) {
            $filter = $request->get('search');
            $appointments = $appointments->whereHas('citizen', function ($query) use ($filter) {
                return $query->where('name', 'like', '%'.$filter.'%');
            });
        }

        $appointments = $appointments->paginate(5);

        $duplicates = DB::table('appointments')
            ->select('appoint_date')
            ->groupBy('appoint_date')
            ->havingRaw('COUNT(*) > 1')
            ->get()
            ->pluck('appoint_date')
            ->toArray();

        return view('lawyer_home', compact(
            'appointments',
            'duplicates',
            'filter'
        ));
    }
}
