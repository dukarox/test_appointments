<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Http\Requests\AppointmentRequest;
use App\Http\Requests\AppointmentEditRequest;
use Carbon\Carbon;
use Auth;

class AppointmentsController extends Controller
{
    public function save(AppointmentRequest $request)
    {
        $requestDateTime = $request->get('date') . ' ' . $request->get('time');
        $appointmentDateTime = Carbon::createFromFormat('Y-m-d H:i', $requestDateTime);

        $modelAttributes = [
            'appoint_date' => $appointmentDateTime,
            'citizen_id' => Auth::user()->id,
            'lawyer_id' => $request->get('lawyer_id'),
        ];

        Appointment::create($modelAttributes);

        return redirect()->back()->with('message', 'Your appointment is sent!');
    }

    public function edit(AppointmentEditRequest $request)
    {
        $requestDateTime = $request->get('date') . ' ' . $request->get('time');
        $appointmentDateTime = Carbon::createFromFormat('Y-m-d H:i', $requestDateTime);

        $appointment = Appointment::find($request->get('appointment_id'));

        if (!$appointment) {
            return redirect()->back()->with('message', 'The appointment is not found!');
        }

        $appointment->appoint_date = $requestDateTime;
        $appointment->status = Appointment::APPROVED;
        $appointment->save();

        return redirect()->back()->with('message', 'The appointment is rescheduled!');
    }

    public function approve($id)
    {
        $appointment = Appointment::find($id);
        $appointment->status = Appointment::APPROVED;
        $appointment->save();
        return redirect()->back()->with('message', 'You approved an appointment!');
    }

    public function decline($id)
    {
        $appointment = Appointment::find($id);
        $appointment->status = Appointment::DECLINED;
        $appointment->save();
        return redirect()->back()->with('message', 'You declined an appointment!');
    }
}
