<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use KyslikColumnSortableSortable;

class Appointment extends Model
{
    use Sortable;

    const WAITING = 0;
    const APPROVED = 1;
    const DECLINED = 2;

    protected $table = "appointments";

    protected $fillable = [
        'appoint_date',
        'citizen_id',
        'lawyer_id',
        'status',
    ];

    public $sortable = [
        'appoint_date',
        'citizen_id',
        'lawyer_id',
        'status',
    ];

    public function lawyerName()
    {
        return $this->lawyer->name;
    }

    public function citizenName()
    {
        return $this->citizen->name;
    }

    public function lawyer()
    {
        return $this->hasOne('App\User', 'id', 'lawyer_id');
    }

    public function citizen()
    {
        return $this->hasOne('App\User', 'id', 'citizen_id');
    }

}
